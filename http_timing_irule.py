import json

def option1():
    whs = (ms_client_accepted - flow_init) / 1000
    ssl = (ms_clientssl_hs - ms_clientssl_hello) / 1000
    lb_selected = (ms_lb_selected - ms_clientssl_hs) / 1000
    server_conn = (ms_server_connected - ms_lb_selected) / 1000
    http_req = (ms_http_request - ms_server_connected) / 1000
    asm_req = (ms_asm_request_done - ms_http_request) / 1000
    http_request_release = (ms_http_request_release - ms_http_request) / 1000
    http_request_send = (ms_http_request_send - ms_asm_request_done) / 1000
    http_resp = (ms_http_response - ms_http_request_send) / 1000
    http_resp_release = (ms_http_response_release - ms_http_response) / 1000
    return whs, ssl, lb_selected, server_conn, http_req, asm_req, http_request_release, http_request_send, http_resp, http_resp_release

def option2():
    whs = (ms_client_accepted - flow_init) / 1000
    ssl = (ms_clientssl_hs - ms_clientssl_hello) / 1000
    lb_selected = (ms_lb_selected - ms_clientssl_hs) / 1000
    server_conn = (ms_server_connected - ms_lb_selected) / 1000
    http_req = (ms_http_request - ms_server_connected) / 1000
    asm_req = "NA"
    http_request_release = (ms_http_request_release - ms_http_request) / 1000
    http_request_send = (ms_http_request_send - ms_http_request_send) / 1000
    http_resp = (ms_http_response - ms_http_request_send) / 1000
    http_resp_release = (ms_http_response_release - ms_http_response) / 1000
    return whs, ssl, lb_selected, server_conn, http_req, asm_req, http_request_release, http_request_send, http_resp, http_resp_release

def print_result():
    print(f"""




      CLIENT                                                     SILVERLINE                                        SERVER


     ┌=======┐                   ┌────────────────────────────────────────────────────────────────────┐           ┌───────┐
     ││     ││       |  ---->    │                                                                    │  ---->    │_===_°°│ Response from the server
     └=======┘       |  <----    │ 3WHS .....{whs1}          
   |[=== -- o ]|     |  ---->    │                                  |  Server connected ....{server1} 
    '---------'         ---------│-----                             |                        ---------│-----      └───────┘
 [::::::::::: :::]   |  ---->    │                                  |  HTTP req release ....{http_d1}
                     |  <----    │                                  |  HTTP req send ........{http_r_s1}
                     |  ---->    │  Client SSL ....{ssl1}                  
                        ---------│-----                                                               │                   
                                 │                                                                    │
                                 │             ────> HTTP request ...................{http_req1}                               
                                 │              └───> LB selected ...................{lbl1}
                                 │               └───> asm_request_done .............{asm1}
                                 │                                                                    │
                        ---------│-----                             |                        ---------│-----         
                         <----   │                                  |   HTTP response.......{http_resp1}
                                 │  HTTP response release .... {http_rel1}      
                                 └────────────────────────────────────────────────────────────────────┘  

    """)
    print(f"  Total time connection customer <------> Silverline......{whs + ssl + http_req}")
    print(f"  Total time processing the request within Silverline......{lb_selected + server_conn + http_resp_release + http_request_release + http_request_send}")
    print(f"  Total time receiving the response from the server .......{http_resp}")
    print(f"   [+] Path requested:  {path}")
    print(f"   [+] Response content length:  {len}")


if __name__ == '__main__':
    a = input(">> ")
    b = input()
    c = input()
    d = input()
    e = input()
    f = input()
    g = input()
    h = input()
    i = input()
    j = input()
    k = input()
    l = input()
    m = input()
    n = input()
    o = input()
    p = input()
    q = input()
    r = input()
    s = input()
    t = input()

    l = [a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t]

    with open(f"http_timing.json", "w") as file:
        file.write("{")
        for x in l:
            file.write(x)
        file.write("}")


    with open(f"http_timing.json", "r") as file:
        try:
            d_json = json.load(file)
            dir = d_json["data"]
            flow_init = int(dir.get("flow_init"))
            ms_client_accepted = int(dir.get("ms_client_accepted"))
            ms_clientssl_hello = int(dir.get("ms_clientssl_hello"))
            ms_clientssl_hs = int(dir.get("ms_clientssl_hs"))
            ms_http_request= int(dir.get("ms_http_request"))
            ms_lb_selected = int(dir.get("ms_lb_selected"))
            ms_server_connected = int(dir.get("ms_server_connected"))
            ms_http_request_release = int(dir.get("ms_http_request_release"))
            ms_http_request_send = int(dir.get("ms_http_request_send"))
            ms_http_response_release = int(dir.get("ms_http_response_release"))
            ms_http_response = int(dir.get("ms_http_response"))
            path = dir.get("http_request_name")
            len = dir.get("http_resp_content_len")
            ms_asm_request_done = int(dir.get("ms_asm_request_done"))
            whs, ssl, lb_selected, server_conn, http_req, asm_req, http_request_release, http_request_send, http_resp, http_resp_release = option1()

        except ValueError:
            ms_asm_request_done = "NA"
            whs, ssl, lb_selected, server_conn, http_req, asm_req, http_request_release, http_request_send, http_resp, http_resp_release = option2()

        whs1 = "{:<23}|                                 │  <----    │_____°°│".format(whs)
        ssl1 = "{:<12}     |                        ---------│----- ".format(ssl)
        lbl1 = "{:<17}│".format(lb_selected)
        server1 = "{:<10}│  <----    │ === °°│".format(server_conn)
        http_d1 = "{:<10}│".format(http_request_release)
        http_r_s1 = "{:<9}│  ---->".format(http_request_send)
        asm1 = "{:<17}│".format(asm_req)
        http_req1 = "{:<17}│".format(http_req)
        http_rel1 = "{:<39}│".format(http_resp_release)
        http_resp1 = "{:<10}│  <----".format(http_resp)

print_result()
